#!/bin/bash

# copied from
# https://github.com/icshwi/etherlabmaster/blob/master/configure/E3/CONFIG_OPTIONS_BUILDER

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"
CONFIG_FILE="${DIR}/etherlab.conf"
DEVICE_IS_SELECTED=0

if [ -f ${CONFIG_FILE} ]; then
	. ${CONFIG_FILE}

	## Network Devices
	## Tested
	if [ "${WITH_DEV_GENERIC}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-generic"
		DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-generic"
	fi

	## NOT Tested
	if [ "${WITH_DEV_8139TOO}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-8139too"
		DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-8139too"
	fi

	## NOT Tested
	if [ "${WITH_DEV_E100}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-e100"
		DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-e100"
	fi

	## NOT Tested
	if [ "${WITH_DEV_E1000}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-e1000"
		DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-e1000"
	fi

	## Tested
	if [ "${WITH_DEV_E1000E}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-e1000e"
		DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-e1000e"
	fi

	## NOT Tested
	if [ "${WITH_DEV_IGB}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-igb"
		DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-igb"
	fi

	## NOT Tested
	if [ "${WITH_DEV_R8169}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-r8169"
		DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-r8169"
	fi

	## Tested
	## Native CCAT needs the convert-ccat-to-mfd.patch
	## So, the option should be used as -ccat_netdev instead of -ccat
	## Thursday, November 28 23:42:05 CET 2019, jhlee
	##
	if [ "${WITH_DEV_CCAT}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-ccat_netdev"
	DEVICE_IS_SELECTED=1
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-ccat"
	fi

	## Other Options
	##
	if [ "${ENABLE_STATIC}" = "YES" ]; then
	ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-static=yes"
	else
	ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-static=no"
	fi

	if [ "${ENABLE_SHARED}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-shared=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-shared=no"
	fi

	if [ "${ENABLE_EOE}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-eoe=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-eoe=no"
	fi

	if [ "${ENABLE_CYCLES}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-cycles=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-cycles=no"
	fi

	if [ "${ENABLE_HRTIMER}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-hrtimer=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-hrtimer=no"
	fi

	if [ "${ENABLE_REGALIAS}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-regalias=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-regalias=no"
	fi

	if [ "${ENABLE_TOOL}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-tool=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-tool=no"
	fi

	if [ "${ENABLE_USERLIB}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-userlib=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-userlib=no"
	fi

	if [ "${ENABLE_SII_ASSIGN}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-sii-assign=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-sii-assign=no"
	fi

	if [ "${ENABLE_RT_SYSLOG}" = "YES" ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-rt-syslog=yes"
	else
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-rt-syslog=no"
	fi

	if [ "${WITH_PATCHSET}" = "YES" ]; then
		if [ "${ENABLE_RTMUTEX}" = "YES" ]; then
			ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-rtmutex=yes"
		else
			ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-rtmutex=no"
		fi
	fi

	if [ ! -z "${INSTALL_DIR}" ]; then
		PREFIX=${INSTALL_DIR}
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --prefix=${PREFIX}"
	else
		PREFIX=/usr/local
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --prefix=${PREFIX}"
	fi

	if [ ${DEVICE_IS_SELECTED} = 0 ]; then
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-generic"
		ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-8139too"
	fi
fi

if [ -z "${ETHERLAB_OPTIONS}" ]; then
	ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --enable-generic"
	ETHERLAB_OPTIONS="${ETHERLAB_OPTIONS} --disable-8139too"
	PREFIX=/usr/local
fi
