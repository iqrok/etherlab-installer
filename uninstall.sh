#!/bin/sh

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

# stop ethercat if it's running
if [ -n "$(find /dev -name "*EtherCAT*")" ]; then
	sudo systemctl stop ethercat.service
	sudo /etc/init.d/ethercat stop
fi

ETHERCAT_DIR="${DIR}/ethercat"

# check if ethercat is installed as dkms
DKMS_STATUS="$(dkms status | grep -P "^ethercat")"
if [ -n "${DKMS_STATUS}" ]; then
	DKMS_MODULE_NAME="ethercat"
	DKMS_MODULE_VERSION="$(echo "${DKMS_STATUS}" | sed -r 's#^ethercat/([^, ]+)(.*$)#\1#g')"
	ETHERCAT_DIR="/usr/src/${DKMS_MODULE_NAME}-${DKMS_MODULE_VERSION}"
fi

cd "${ETHERCAT_DIR}" || exit 1
sudo make uninstall-recursive
cd - || exit 1

PREFIX=$(grep -P "prefix = ([a-zA-Z0-9\/]+)" "${ETHERCAT_DIR}/Makefile" | sed -r "s,prefix \= (.*?),\1,g")
if [ -z "${PREFIX}" ]; then
	PREFIX=/usr/local
fi

sudo rm -rf /etc/sysconfig/ethercat*
sudo rm -f /etc/init.d/ethercat \
	/etc/udev/rules.d/99-EtherCAT.rules \
	/etc/bash_completion.d/ethercat-completions \
	"${PREFIX}/etc/ethercat.conf"

# exit if not installed as dkms
[ -z "${DKMS_STATUS}" ] && exit 0

sudo dkms remove --all -m "${DKMS_MODULE_NAME}" -v "${DKMS_MODULE_VERSION}"
sudo rm -rf "${ETHERCAT_DIR}"
