#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ETHERCAT_DIR="${DIR}/ethercat"
ETHERLAB_GIT_URL="https://gitlab.com/etherlab.org/ethercat.git"

source set_params.sh

ARGUMENTS=$@
if [ -z "${ARGUMENTS}" ]; then
	ARGUMENTS="--enable-generic --disable-8139too --prefix=${PREFIX}"
fi

if [ ! -d "${ETHERCAT_DIR}" ]; then
	git clone "${ETHERLAB_GIT_URL}" "${ETHERCAT_DIR}"
fi

pushd "${ETHERCAT_DIR}" || exit 1

git pull

./bootstrap
./configure ${ARGUMENTS}

make all modules

sudo make modules_install install
sudo depmod

MASTER_DEVICE=$(ip -br link | grep -Pv "(LOOPBACK|UNKNOWN)" | grep -Pv "^w" | awk 'NR==1{ print $3 }')
DEVICE_MODULES="generic"

if [ -z "${MASTER_DEVICE}" ]; then
	echo "Network Interface not found!"
	ip -br link
	exit 1
fi

printf "MASTER0_DEVICE=\"${MASTER_DEVICE}\"\n\nDEVICE_MODULES=\"${DEVICE_MODULES}\"\n" | sudo tee ${PREFIX}/etc/sysconfig/ethercat > /dev/null

sudo rm -f /etc/init.d/ethercat
sudo ln -s "${PREFIX}/etc/init.d/ethercat" /etc/init.d/ethercat

# config for /etc/init.d/ethercat
sudo mkdir -p /etc/sysconfig
sudo rm -f /etc/sysconfig/ethercat
sudo ln -s ${PREFIX}/etc/sysconfig/ethercat /etc/sysconfig/ethercat

# config for /sbin/ethercatctl
sudo rm -f ${PREFIX}/etc/ethercat.conf
sudo ln -s ${PREFIX}/etc/sysconfig/ethercat ${PREFIX}/etc/ethercat.conf

echo KERNEL==\"EtherCAT[0-9]*\", SUBSYSTEM==\"EtherCAT\", MODE=\"0666\" | sudo tee /etc/udev/rules.d/99-EtherCAT.rules > /dev/null

sudo udevadm control --reload && sudo udevadm trigger
sudo systemctl daemon-reload

sudo cp ${DIR}/ethercat-completions /etc/bash_completion.d/ethercat-completions

sudo /etc/init.d/ethercat start
sleep 1
ethercat master
sudo /etc/init.d/ethercat stop

popd
