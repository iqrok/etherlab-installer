#!/bin/sh

set -e

if [ "$(id -u)" != "0" ]; then
	echo "Must be run as root!"
	exit 1
fi

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

ARGUMENTS=${@}
GIT_BRANCH="master"

while :; do
	CMDNAME="${1}"
	case "${CMDNAME}" in
		-b|--branch)
			shift
			GIT_BRANCH="${1}"
			ARGUMENTS=$(echo "${ARGUMENTS}"| sed -r "s,${CMDNAME}\s+${1}\s+,,g")
			shift
			;;

		*)
			[ "${#}" = "0" ] && break
			shift
			;;

	esac
done

DKMS_PACKAGE_NAME="ethercat"
DKMS_PACKAGE_VERSION="${GIT_BRANCH}"

ETHERCAT_DIR="/usr/src/${DKMS_PACKAGE_NAME}-${DKMS_PACKAGE_VERSION}"
ETHERLAB_GIT_URL="https://gitlab.com/etherlab.org/ethercat.git"

. ./set_params.sh

if [ -z "${ARGUMENTS}" ]; then
	ARGUMENTS="--enable-generic --disable-8139too --prefix=${PREFIX}"
fi

if [ ! -d "${ETHERCAT_DIR}" ]; then
	git clone "${ETHERLAB_GIT_URL}" \
		--branch "${GIT_BRANCH}" \
		--single-branch "${ETHERCAT_DIR}"
else
	cd "${ETHERCAT_DIR}"
	git pull
	cd -
fi

# DKMS setup
cp -t "${ETHERCAT_DIR}" "${DIR}/dkms/dkms.conf" "${DIR}/dkms/dkms_post_build.sh"
sed -i "s,__DKMS_PACKAGE_VERSION__,${DKMS_PACKAGE_VERSION},g" "${ETHERCAT_DIR}/dkms.conf"
sed -i "s,__ETHERCAT_ARGUMENTS__,${ARGUMENTS},g" "${ETHERCAT_DIR}/dkms.conf"

dkms add -m "${DKMS_PACKAGE_NAME}" -v "${DKMS_PACKAGE_VERSION}"
dkms build -m "${DKMS_PACKAGE_NAME}" -v "${DKMS_PACKAGE_VERSION}"
dkms install -m "${DKMS_PACKAGE_NAME}" -v "${DKMS_PACKAGE_VERSION}"

# ethercat bash completions
BASH_COMPLETION_DST="/etc/bash_completion.d/ethercat-completions"
BASH_COMPLETION_SRC="${ETHERCAT_DIR}/script/ethercat.bash_completion"

if [ ! -f "${BASH_COMPLETION_SRC}" ]; then
	# use homemade script instead
	BASH_COMPLETION_SRC="${DIR}/ethercat-completions"
fi

cp "${BASH_COMPLETION_SRC}" "${BASH_COMPLETION_DST}"
chmod a+x "${BASH_COMPLETION_DST}"

# try ethercat
/usr/local/sbin/ethercatctl start
sleep 1
ethercat master
/usr/local/sbin/ethercatctl stop
