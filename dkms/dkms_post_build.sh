#!/bin/bash

set -x
DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

# sync build dir into source dir, so we can use it later to uninstall
SRC_DIR="$(readlink "${DIR}/../source")"
rsync -avu "${DIR}/" "${SRC_DIR}"

PREFIX="$(grep -P "prefix = ([a-zA-Z0-9\/]+)" "${DIR}/Makefile" | sed -r "s,prefix \= (.*?),\1,g")"
if [ -z "${PREFIX}" ]; then
	PREFIX="/usr/local"
fi

make install

MASTER_DEVICE="$(ip -br link | grep -Pv "^w|LOOPBACK|UNKNOWN" |  awk 'NR==1{ print $3 }')"
DEVICE_MODULES="generic"

if [ -z "${MASTER_DEVICE}" ]; then
	echo "Network Interface not found!"
	ip -br link
	exit 1
fi

printf "MASTER0_DEVICE=\"${MASTER_DEVICE}\"\n\nDEVICE_MODULES=\"${DEVICE_MODULES}\"\n" | tee ${PREFIX}/etc/sysconfig/ethercat > /dev/null

rm -f /etc/init.d/ethercat
ln -s "${PREFIX}/etc/init.d/ethercat" /etc/init.d/ethercat

# config for /etc/init.d/ethercat
mkdir -p /etc/sysconfig
rm -f /etc/sysconfig/ethercat
ln -s ${PREFIX}/etc/sysconfig/ethercat /etc/sysconfig/ethercat

# config for /sbin/ethercatctl
rm -f ${PREFIX}/etc/ethercat.conf
ln -s ${PREFIX}/etc/sysconfig/ethercat ${PREFIX}/etc/ethercat.conf

echo KERNEL==\"EtherCAT[0-9]*\", SUBSYSTEM==\"EtherCAT\", MODE=\"0666\" | tee /etc/udev/rules.d/99-EtherCAT.rules > /dev/null

udevadm control --reload && udevadm trigger
systemctl daemon-reload
